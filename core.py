import sys
import PyQt5.QtWidgets as qt
import ui
import ctrl
import mysql.connector


class App(object):
    """Main application object.
    Responsible for doing all logic and calculations, 
    controlling app and communicating with database.
    """
    _qt_app = qt.QApplication(sys.argv)

    def __init__(self, settings):
        """App object constructor
        """
        # Create window and configure it
        # TODO: Pass dict with settings
        # TODO: make window an object (?)
        self._settings = settings
        self._window = qt.QMainWindow()
        self._window.setWindowTitle(settings['window']['title'])
        self._window.resize(settings['window']['width'], settings['window']['heigth'])

    def run(self):
        """Run application
        """
        self._qt_app.exec_()

    def setUI(self, name):
        """Set current user interface

        Args:
            name (string): name of UI
        """
        # Create new UI and link controller to it
        # TODO: use enums instead of strings
        if name == 'login':
            self.ui = ui.Login()
            self.ctrl = ctrl.LoginCtrl(self, self.ui)
        elif name == 'main':
            self.ui = ui.Main()
            self.ctrl = ctrl.MainCtrl(self, self.ui)
        else:
            # No correct UI name given
            raise RuntimeError('no ui name found')

        # Set UI to the main window
        self._window.setCentralWidget(self.ui)

    def show(self):
        """Show window on screen
        """
        self._window.show()
