
import mysql.connector as sql

    
class Database:
    """Database connector.
    """    
    def connect(self, user, passwd):
        """Connect to the database

        Args:
            user (string): user name
            passwd (string): user password
        """
        # self._con = QSqlDatabase.addDatabase("QMYSQL")
        # print(self._con.isOpen())
        try:
            self._db_con = sql.connect(
                host="localhost",
                user=user,
                password=passwd,
                database="pizzeria")
        except sql.Error as e:
            print(e)

    def query(self, query):
        """Execute sql query and return rows.

        Args:
            query (string): sql query

        Returns:
            list of tuples: fields in row
        """
        with self._db_con.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
            # for row in result:
                # print(row)
            return result