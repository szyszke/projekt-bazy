import PyQt5.QtWidgets as qt
import core
import ui


class Controller():
    """App controller class
    """

    def __init__(self, app, ui):
        """Create Controller object

        Args:
            app (App): handle to App object
            ui (QWidget): handle to ui object
        """
        # Set handles for app and ui objects
        self._app = app
        self._ui = ui

    def setup(self):
        """Configure UI with application
        """
        raise NotImplementedError("Controller is abstract class.")


class LoginCtrl(Controller):
    """Login UI Controller Class
    """

    def __init__(self, app, ui):
        """Create LoginCtrl object

        Args:
            app (App): handle to App object
            ui (QWidget): handle to ui object
        """
        super().__init__(app, ui)
        self.setup()

    def _loginHandle(self):
        """Login into application
        """
        # TODO: Handle login and authorization
        self._app.setUI('main')

    def setup(self):
        """Configure UI with application
        """
        # Connect functions to the buttons
        self._ui.buttons['login'].clicked.connect(self._loginHandle)


class MainCtrl(Controller):
    """Main UI Controller Class
    """

    def __init__(self, app, ui):
        """Create MainCtrl object

        Args:
            app (App): handle to App object
            ui (QWidget): handle to ui object
        """
        super().__init__(app, ui)
        self.setup()

    def setup(self):
        """Configure UI with application
        """
        pass
